package com.example.demo.repository;

import java.util.List;

import org.json.JSONObject;
import org.springframework.stereotype.Component;
import com.example.demo.entities.Trade;


@Component
public interface TradeRepository {
	public List<JSONObject> getAllTrades();
	public List<JSONObject> getAllTradesByUser(int userid);
	public List<JSONObject> getAllByTickerName(String tickername);
	public List<JSONObject> getCompaniesByName(String cname);
	public Trade buyTrade(Trade trade);
	public Trade sellTrade(Trade trade);
	public Trade autobuyTrade(Trade trade);
	public Trade autosellTrade(Trade trade);
}
