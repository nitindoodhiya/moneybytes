package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.example.demo.entities.User;

@Repository
public class MySQLUserRepository implements UserRepository {
	@Autowired
	JdbcTemplate  template;
	
	@Override
	public List<JSONObject> getAllUsers() {
		String sql = "SELECT id,username,fname,lname,email FROM Users";
		return template.query(sql, new UserJSONRowMapper());
	}

	@Override
	public List<JSONObject> getUsersByUsername(String username) {
		String sql = "SELECT * FROM Users  Where username like ? ";
		username = "%" + username + "%";
		return  template.query(sql,  new UserJSONRowMapper(),username);
	}

	@Override
	public List<JSONObject> getUsersByEmail(String email) {
		String sql = "SELECT * FROM Users  Where email = ? ";
		return  template.query(sql,  new UserJSONRowMapper(),email);		
		
	}
	@Override
	public User editUser(User user) {
		String sql = "UPDATE Users SET username = ?, fname = ?, lname = ?, email = ? WHERE id = ?";
		template.update(sql,user.getUsername(),user.getFname(),user.getLname(), user.getEmail(),user.getId());
		return user;
	}

	@Override
	public int deleteUserById(int id) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public String deleteUserByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public User addUser(User user) {
		String sql = "INSERT INTO Users(username, fname,lname,email,password) " +
				"VALUES(?,?,?,?,?)";
		template.update(sql,user.getUsername(),user.getFname(),user.getLname(), user.getEmail(),user.getPassword());
		return user;		
	}
	@Override
	public List<JSONObject> getUserByEmailIdAndPassword(User user) {
		String sql = "SELECT * FROM Users  Where email = ? and password = ?";
		return template.query(sql,  new UserJSONRowMapper(),user.getEmail(),user.getPassword());	
	}
	class UserRowMapper implements RowMapper<User>{

		@Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new User(rs.getInt("id"),
					rs.getString("username"),
					rs.getString("fname"),rs.getString("lname"), rs.getString("email"), null);
		}
	}
	class UserJSONRowMapper implements RowMapper<JSONObject>{

		@Override
		public JSONObject mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new JSONObject(new User(rs.getInt("id"),
					rs.getString("username"),
					rs.getString("fname"),rs.getString("lname"), rs.getString("email"), null));
		}
	}
	
}
