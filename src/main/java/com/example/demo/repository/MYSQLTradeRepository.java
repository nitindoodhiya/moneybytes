package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Trade;
@Repository
public class MYSQLTradeRepository implements TradeRepository{
	@Autowired
	JdbcTemplate  template;
	
	@Override
	public List<JSONObject> getAllTrades() {
		String sql = "SELECT * FROM trade";
		return template.query(sql, new TradeJSONRowMapper());
	}

	@Override
	public List<JSONObject> getAllByTickerName(String tickername) {
		String sql = "SELECT * FROM trade where stockTicker = ?";
		return template.query(sql, new TradeJSONRowMapper(),tickername);		
	}

	@Override
	public List<JSONObject> getCompaniesByName(String cname) {
		String sql = "SELECT * FROM tickerList  Where name like ? ";
		cname = "%" + cname + "%";
		
		return template.query(sql,new TickerRowMapper(),cname);
		
	}

	@Override
	public Trade buyTrade(Trade trade) {
		String sql = "INSERT INTO trade(stockTicker, userID, price, volume, buyOrSell, statusCode, time) " +
				"VALUES(?,?,?,?,?,?,?)";
		template.update(sql,trade.getStockTicker(),trade.getUserID(),trade.getPrice(),trade.getVolume(),"BUY",trade.getStatusCode(),trade.getTime());
		return trade;		
	}

	@Override
	public Trade sellTrade(Trade trade) {
		String sql = "INSERT INTO trade(stockTicker, userID, price, volume, buyOrSell, statusCode, time) " +
				"VALUES(?,?,?,?,?,?,?)";
		template.update(sql,trade.getStockTicker(),trade.getUserID(),trade.getPrice(),trade.getVolume(),"SELL",trade.getStatusCode(),trade.getTime());
		return trade;		
	}

	@Override
	public Trade autobuyTrade(Trade trade) {
		String sql = "INSERT INTO trade(stockTicker, userID, price, volume, buyOrSell, statusCode, time) " +
				"VALUES(?,?,?,?,?,?,?)";
		template.update(sql,trade.getStockTicker(),trade.getUserID(),trade.getPrice(),trade.getVolume(),"BUY",5,trade.getTime());
		return trade;		
	}

	@Override
	public Trade autosellTrade(Trade trade) {
		String sql = "INSERT INTO trade(stockTicker, userID, price, volume, buyOrSell, statusCode, time) " +
				"VALUES(?,?,?,?,?,?,?)";
		template.update(sql,trade.getStockTicker(),trade.getUserID(),trade.getPrice(),trade.getVolume(),"SELL",5,trade.getTime());
		return trade;		
	}


	@Override
	public List<JSONObject> getAllTradesByUser(int userid) {

		String sql = "SELECT * FROM trade where userID = ?";
		return template.query(sql, new TradeJSONRowMapper(),userid);
	}
	
	class TradeRowMapper implements RowMapper<Trade>{

		@Override
		public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Trade(rs.getInt("tradeID"),
					rs.getString("stockTicker"),
					rs.getInt("userID"),
					rs.getDouble("price"),
					rs.getInt("volume"),
					rs.getString("buyOrSell"),
					rs.getString("statusCode"),
					Long.parseLong(rs.getString("time")) );
		}
	}
	class TradeJSONRowMapper implements RowMapper<JSONObject>{

		@Override
		public JSONObject mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new JSONObject(new Trade(rs.getInt("tradeID"),
					rs.getString("stockTicker"),
					rs.getInt("userID"),
					rs.getDouble("price"),
					rs.getInt("volume"),
					rs.getString("buyOrSell"),
					rs.getString("statusCode"),
					Long.parseLong(rs.getString("time"))) );
		}
	}

	class TickerRowMapper implements RowMapper<JSONObject>{

		@Override
		public JSONObject mapRow(ResultSet rs, int rowNum) throws SQLException {
			JSONObject json = new JSONObject();
			json.put("symbol",rs.getString("symbol"));
			json.put("name", rs.getString("name"));
			json.put("country", rs.getString("country"));
			json.put("industry", rs.getString("industry"));
			return json;
		}
	}
}
