/**
 * 
 */
package com.example.demo.repository;

import java.util.List;

import org.json.JSONObject;
import org.springframework.stereotype.Component;
import com.example.demo.entities.User;

@Component
public interface UserRepository {
	public List<JSONObject> getAllUsers();
	public List<JSONObject> getUsersByUsername(String username);
	public List<JSONObject> getUsersByEmail(String email);
	public User editUser(User user);
	public int deleteUserById(int id);
	public String deleteUserByUsername(String username);
	public User addUser(User user);
	public List<JSONObject> getUserByEmailIdAndPassword(User user);
}
