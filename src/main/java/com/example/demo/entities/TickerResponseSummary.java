package com.example.demo.entities;

import java.sql.Time;

public class TickerResponseSummary {
		private Double open;
		private Double high;
		private Double low;
		private Double last;
		private Double close;
		private Double volume;
		private Time date;
		private String symbol;
		private String exchange;
		
		public TickerResponseSummary() {
			super();
		}

		public TickerResponseSummary(Double open, Double high, Double low, Double last, Double close, Double volume,
				Time date, String symbol, String exchange) {
			super();
			this.open = open;
			this.high = high;
			this.low = low;
			this.last = last;
			this.close = close;
			this.volume = volume;
			this.date = date;
			this.symbol = symbol;
			this.exchange = exchange;
		}

		public Double getOpen() {
			return open;
		}

		public void setOpen(Double open) {
			this.open = open;
		}

		public Double getHigh() {
			return high;
		}

		public void setHigh(Double high) {
			this.high = high;
		}

		public Double getLow() {
			return low;
		}

		public void setLow(Double low) {
			this.low = low;
		}

		public Double getLast() {
			return last;
		}

		public void setLast(Double last) {
			this.last = last;
		}

		public Double getClose() {
			return close;
		}

		public void setClose(Double close) {
			this.close = close;
		}

		public Double getVolume() {
			return volume;
		}

		public void setVolume(Double volume) {
			this.volume = volume;
		}

		public Time getDate() {
			return date;
		}

		public void setDate(Time date) {
			this.date = date;
		}

		public String getSymbol() {
			return symbol;
		}

		public void setSymbol(String symbol) {
			this.symbol = symbol;
		}

		public String getExchange() {
			return exchange;
		}

		public void setExchange(String exchange) {
			this.exchange = exchange;
		}
		
		
		
}