package com.example.demo.entities;

public class TickerAPI {
	private String symbol;
	private String interval;
	private String date_from;
	private String date_to;
	private Integer limit;
	private Integer offset;
	private String sort;
	public TickerAPI() {
	}
	
	public TickerAPI(String symbol, String interval, String date_from, String date_to, Integer limit, Integer offset,
			String sort) {
		super();
		this.symbol = symbol;
		this.interval = interval;
		this.date_from = date_from;
		this.date_to = date_to;
		this.limit = limit;
		this.offset = offset;
		this.sort = sort;
	}

	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getInterval() {
		return interval;
	}
	public void setInterval(String interval) {
		this.interval = interval;
	}
	public String getDate_from() {
		return date_from;
	}
	public void setDate_from(String date_from) {
		this.date_from = date_from;
	}
	public String getDate_to() {
		return date_to;
	}
	public void setDate_to(String date_to) {
		this.date_to = date_to;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public Integer getOffset() {
		return offset;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	
}
