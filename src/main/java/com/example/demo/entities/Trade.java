package com.example.demo.entities;


public class Trade {
	private int tradeID;
	private String stockTicker;
	private int userID;
	private double price;
	private int volume;
	private String buyOrSell;
	private String statusCode;
	private long time;
		
	public Trade() {
		
	}
	
	public Trade(int tradeID, String stockTicker, int userID, double price, int volume, String buyOrSell,
			String statusCode, long time) {
		super();
		this.tradeID = tradeID;
		this.stockTicker = stockTicker;
		this.userID = userID;
		this.price = price;
		this.volume = volume;
		this.buyOrSell = buyOrSell;
		this.statusCode = statusCode;
		this.time = time;
	}
	
	public long getTime() {
		return time;
	}

	public void setTime(long l) {
		this.time = l;
	}
	
	public int getTradeID() {
		return tradeID;
	}

	public void setTradeID(int tradeID) {
		this.tradeID = tradeID;
	}

	public String getStockTicker() {
		return stockTicker;
	}

	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public String getBuyOrSell() {
		return buyOrSell;
	}

	public void setBuyOrSell(String buyOrSell) {
		this.buyOrSell = buyOrSell;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

}
