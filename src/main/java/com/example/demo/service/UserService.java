package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.example.demo.entities.User;
import com.example.demo.repository.UserRepository;
import com.google.gson.Gson;

@Service
public class UserService {
	@Autowired
	private UserRepository userrepository;
    public static Logger logger = LogManager.getLogger(UserService.class);

	public List<JSONObject> getAllUsers(){
		JSONObject resultJson = new JSONObject();
		try {
			List<JSONObject>list = userrepository.getAllUsers();
			resultJson.put("result", new Gson().toJson(list));
			resultJson.put("status","success");
			resultJson.put("message", "All Users");
	        logger.info("getting all users "+ resultJson.toString());
	        return list;
		}
		catch(Exception ex) {
			resultJson.put("status","failure");
			resultJson.put("err_message", "internal");
	        logger.error("getting all users gave Exception "+ ex.getMessage());
	        List<JSONObject> l =new ArrayList<JSONObject>();
	        l.add(resultJson);
	        return l;
		}
	}
	public List<JSONObject> getUsers(String username) {
		JSONObject resultJson = new JSONObject();
		try {
			
			List<JSONObject>list =  userrepository.getUsersByUsername(username);
			resultJson.put("result", new Gson().toJson(list));
			resultJson.put("status","success");
			resultJson.put("message", "users with simiar username");
	        logger.info("getting all usernames with similar name: "+username+" ,result: "+ resultJson.toString());
	        return list;
		}
		catch(Exception ex) {
			resultJson.put("status","failure");
			resultJson.put("err_message", "internal");
	        logger.error("getting all usernames with similar name: "+username+" ,result gave Exception: "+ ex.getMessage());
	        List<JSONObject> l =new ArrayList<JSONObject>();
	        l.add(resultJson);
	        return l;
		}
	}
	public JSONObject saveUser(User user) {
		
		JSONObject resultJson = new JSONObject();
		try {
			
			User user_temp =   userrepository.editUser(user);
			resultJson.put("result", new Gson().toJson(user_temp));
			resultJson.put("status","success");
			resultJson.put("message", "edited successfully");
	        logger.info("saving user: "+new JSONObject(user).toString()+" ,result: "+ resultJson.toString());

		}
		catch(Exception ex) {
			resultJson.put("status","failure");
			resultJson.put("err_message", "internal");
	        logger.error("saving user: "+new JSONObject(user).toString()+" ,result Exception: "+ ex.getMessage());
		}
		return resultJson;
	}
	public JSONObject newUser(User user) {
		JSONObject resultJson = new JSONObject();
		try{
			EmailValidator validator = EmailValidator.getInstance();
			if(validator.isValid(user.getEmail())==false) {
				resultJson.put("status", "failure");
				resultJson.put("err_message", "Invalid Email");
				return resultJson;

			}
			if(userrepository.getUsersByUsername(user.getUsername()).size() > 0 ) {
				resultJson.put("status", "failure");
				resultJson.put("err_message", "Username already exists");
				return resultJson;
			} 
			if(userrepository.getUsersByEmail(user.getEmail()).size() > 0 ) {
				resultJson.put("status", "failure");
				resultJson.put("err_message", "email already exists");
				return resultJson;
			} 
			userrepository.addUser(user);
			resultJson.put("status", "success");
			resultJson.put("message", "user successfully added");
	        logger.info("creating user: "+new JSONObject(user).toString()+" ,result: "+ resultJson.toString());
	        return new JSONObject(user);
		} catch(Exception ex) {
			resultJson.put("status", "failure");
			resultJson.put("err_message", "error creating user");
	        logger.error("creating user: "+new JSONObject(user).toString()+" ,result Exception: "+ ex.getMessage());
		
		}
		return resultJson;
	}
	public String deleteUserByUsername(String username) {
		return userrepository.deleteUserByUsername(username);
	}
	public int deleteUserById(int id) {
		return userrepository.deleteUserById(id);
	}
	public List<JSONObject> getUserByEmailIdandPassword(User user){
		JSONObject resultJson = new JSONObject();	
		
		try {
			String email = user.getEmail();
			String password = user.getPassword();
			List<JSONObject> userobj = null;

			if(email != null && password!=null) {
				userobj = userrepository.getUserByEmailIdAndPassword(user);
			}
			
			if(userobj == null) {
				throw new Exception("Bad Credentials");
				
			}
			
			resultJson.put("result", new Gson().toJson(userobj));
			resultJson.put("status","success");
	        logger.info("loging in user: "+new JSONObject(user).toString()+" ,result: "+ resultJson.toString());
	        return userobj;
		}
		catch(Exception ex) {
			resultJson.put("status","failure");
			resultJson.put("err_message", "internal");
	        logger.error("adding user: "+new JSONObject(user).toString()+" ,result Exception: "+ ex.getMessage());
	        List<JSONObject> l =new ArrayList<JSONObject>();
	        l.add(resultJson);
	        return l;
		}
	}
}
