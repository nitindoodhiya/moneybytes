package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.sql.Timestamp;
import java.util.Calendar;  
import java.time.*;  

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.controller.TradeController;
import com.example.demo.entities.Trade;

import com.example.demo.repository.TradeRepository;
import com.google.gson.Gson;

@Service
public class TradeService {

	@Autowired
	private TradeRepository tradeRepository;
    public static Logger logger = LogManager.getLogger(TradeService.class);
    
	public List<JSONObject> getAllTrades(){
		JSONObject resultJson = new JSONObject();
		try {
			List<JSONObject>list = tradeRepository.getAllTrades();
//			resultJson.put("result", new Gson().toJson(list));
//			resultJson.put("status","success");
//			resultJson.put("message", "All Trades");
	        logger.info("getting all Traddes done by all users "+ resultJson.toString());

			return list;
		}
		catch(Exception ex) {
			resultJson.put("status","failure");
			resultJson.put("err_message", "internal");
	        logger.error("getting all Traddes done by all users "+ ex.getMessage());
	        List<JSONObject> l =new ArrayList<JSONObject>();
	        l.add(resultJson);
	        return l;
		}
	}
	
	public List<JSONObject> getCompaniesByName(String cname) {
		JSONObject resultJson = new JSONObject();
		try {
			
			List<JSONObject>list =  tradeRepository.getCompaniesByName(cname);
			resultJson.put("result", new Gson().toJson(list));
			resultJson.put("status","success");
			resultJson.put("message", "Companies with similar  name");
	        logger.info("getting all companies similar to name "+cname+ " result "+ resultJson.toString());
	        return list;
		}
		catch(Exception ex) {
			resultJson.put("status","failure");
			resultJson.put("err_message", "internal");
	        logger.error("getting all companies similar to name "+cname+ " result "+ ex.getMessage());
	        List<JSONObject> l =new ArrayList<JSONObject>();
	        l.add(resultJson);
	        return l;
		
		}
	}
	
	
	public List<JSONObject> getAllByTickerName(String tickername) {
		JSONObject resultJson = new JSONObject();
		try {
			
			List<JSONObject>list =  tradeRepository.getAllByTickerName(tickername);
			System.out.println(list.size());
			resultJson.put("result", new Gson().toJson(list));
			resultJson.put("status","success");
			resultJson.put("message", "All trades with given ticker name");
	        logger.info("All trades with given ticker name "+tickername+ ", result "+ resultJson.toString());

	        return list;
		}
		catch(Exception ex) {
			resultJson.put("status","failure");
			resultJson.put("err_message", "internal");
	        logger.error("All trades with given ticker name "+tickername+ ", result "+ ex.getMessage());
	        List<JSONObject> l =new ArrayList<JSONObject>();
	        l.add(resultJson);
	        return l;
		
		}
	}
	
	
	public List<JSONObject> getAllByTradesByUser(int userid) {
		JSONObject resultJson = new JSONObject();
		try {
			
			List<JSONObject>list =  tradeRepository.getAllTradesByUser(userid);
			resultJson.put("result", new Gson().toJson(list));
			resultJson.put("status","success");
			resultJson.put("message", "Your Trades");
	        logger.info("All tradesby user "+userid+ ", result "+ resultJson.toString());
	        return list;
			
		}
		catch(Exception ex) {
			resultJson.put("status","failure");
			resultJson.put("err_message", "internal");
	        logger.error("All tradesby user "+userid+ ", result Exception "+ ex.getMessage());
	        List<JSONObject> l =new ArrayList<JSONObject>();
	        l.add(resultJson);
	        return l;
		
		}
	}

	
	
	public JSONObject buyTrade(Trade trade) {
	    
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		long time = cal.getTimeInMillis();
		trade.setTime(time);
		trade.setBuyOrSell("BUY");
		JSONObject resultJson = new JSONObject();
		try {
			
			Trade trade_temp =   tradeRepository.buyTrade(trade);
			resultJson.put("result", new Gson().toJson(trade_temp));
			resultJson.put("status","success");
			resultJson.put("message", "BUY Initiated!");
	        logger.info("buy trade "+ new JSONObject(trade).toString() + ", result "+ resultJson.toString());

		}
		catch(Exception ex) {
			resultJson.put("status","failure");
			resultJson.put("err_message", "internal");
	        logger.error("buy trade "+ new JSONObject(trade).toString() + ", result Exception: "+ ex.getMessage());
		}
		return resultJson;
	}
	
public JSONObject sellTrade(Trade trade) {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		long time = cal.getTimeInMillis();
		trade.setTime(time);
		trade.setBuyOrSell("SELL");
		JSONObject resultJson = new JSONObject();
		try {
			Trade trade_temp =   tradeRepository.sellTrade(trade);
			resultJson.put("result", new Gson().toJson(trade_temp));
			resultJson.put("status","success");
			resultJson.put("message", "SELL Successfull!");
	        logger.info("sell trade "+ new JSONObject(trade).toString() + ", result "+ resultJson.toString());

		}
		catch(Exception ex) {
			resultJson.put("status","failure");
			resultJson.put("err_message", "internal");
	        logger.error("sell trade "+ new JSONObject(trade).toString() + ", result Exception: "+ ex.getMessage());
		}
		return resultJson;
	}
public JSONObject autosellTrade(Trade trade) {
	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
	long time = cal.getTimeInMillis();
	trade.setTime(time);
	trade.setBuyOrSell("autoSELL");
	JSONObject resultJson = new JSONObject();
	try {
		Trade trade_temp =   tradeRepository.autosellTrade(trade);
		resultJson.put("result", new Gson().toJson(trade_temp));
		resultJson.put("status","success");
		resultJson.put("message", "SELL Successfull!");
        logger.info("sell trade "+ new JSONObject(trade).toString() + ", result "+ resultJson.toString());

	}
	catch(Exception ex) {
		resultJson.put("status","failure");
		resultJson.put("err_message", "internal");
        logger.error("sell trade "+ new JSONObject(trade).toString() + ", result Exception: "+ ex.getMessage());
	}
	return resultJson;
}


public JSONObject autobuyTrade(Trade trade) {
	Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	trade.setTime(timestamp.getTime());		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
	long time = cal.getTimeInMillis();
	trade.setTime(time);
	trade.setBuyOrSell("autoBUY");
	JSONObject resultJson = new JSONObject();
	try {
		
		Trade trade_temp =   tradeRepository.autobuyTrade(trade);
		resultJson.put("result", new Gson().toJson(trade_temp));
		resultJson.put("status","success");
		resultJson.put("message", "BUY Initiated!");
        logger.info("buy trade "+ new JSONObject(trade).toString() + ", result "+ resultJson.toString());

	}
	catch(Exception ex) {
		resultJson.put("status","failure");
		resultJson.put("err_message", "internal");
        logger.error("buy trade "+ new JSONObject(trade).toString() + ", result Exception: "+ ex.getMessage());
	}
	return resultJson;
}

}

	
