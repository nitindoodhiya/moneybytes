package com.example.demo.service;

import java.util.Arrays;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import com.example.demo.entities.TickerAPI;
import com.example.demo.entities.TickerResponseSummary;

@Service
public class APIService {
	
	@Autowired
	RestTemplate restTemplate;
	
	 @Value("${marketstack_apikey}")
	  private String marketstack_apikey;
	
	public String doAPIRequest(TickerAPI tickerapi){
		String url = "http://api.marketstack.com/v1/intraday?access_key="+marketstack_apikey;
		
		if(tickerapi.getDate_from() != null && tickerapi.getDate_from().equals("null")==false )
			url=url + "&date_from="+tickerapi.getDate_from();	
		if(tickerapi.getDate_to() != null && tickerapi.getDate_to().equals(null)==false)	
			url = url +"&date_to="+tickerapi.getDate_to();
		if(tickerapi.getInterval() != null && !tickerapi.getInterval().equals(null) )
			url = url + "&interval="+tickerapi.getInterval();
		if(tickerapi.getSymbol() != null && !tickerapi.getSymbol().equals(null))
			url = url + "&symbols="+tickerapi.getSymbol();
		if(tickerapi.getSort() != null && !tickerapi.getSort().equals(null) && tickerapi.getSort()!= "")
			url = url + "&sort="+tickerapi.getSort();
		if(tickerapi.getLimit()!=null && tickerapi.getLimit()!=0) {
			url = url + "&limit="+tickerapi.getLimit() + "&offset="+tickerapi.getOffset();
		}
		try {
			String tickersummary = restTemplate.getForObject(url, String.class);
			return tickersummary;
		} catch(Exception ex) {
			return ex.getMessage().toString();
		}
	}
	public String getAllTickers(){
		String url = "http://api.marketstack.com/v1/tickers?access_key="+marketstack_apikey;
		
		try {
			String tickersummary = restTemplate.getForObject(url, String.class);
			return tickersummary;
		} catch(Exception ex) {
			return ex.getMessage().toString();
		}
	}
	
}
