package com.example.demo.controller;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;


import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.User;
import com.example.demo.service.UserService;

@CrossOrigin("*")
@RestController
@RequestMapping("api/users")
public class UserController {
	
	@Autowired
	UserService userservice;
    public static Logger logger = LogManager.getLogger(UserController.class);

	@GetMapping(value = "/working")
	public String testUsers() {
		return "Hello";
	}
	@GetMapping(value = "/all")
	public String getAllUsers() {
        logger.info("get all users");
		return userservice.getAllUsers().toString();
	}
	@PostMapping(value = "/")
	public String addUser(@RequestBody User user) {
        logger.info("adding user " + new JSONObject(user).toString());
		return userservice.newUser(user).toString();
	}
	@GetMapping(value = "/user/{username}")
	public String getUsersByUsername(@PathVariable("username") String username) {
        logger.info("getting user by username " + username);

		return userservice.getUsers(username).toString();
	}
	@PutMapping(value = "/")
	public String editUser(@RequestBody User user) {
        logger.info("editting user  " + new JSONObject(user).toString());

		return userservice.saveUser(user).toString();
	}
	@PostMapping(value = "/login")
	public String loginUser(@RequestBody User user) {
        logger.info("user logging in " + new JSONObject(user).toString());

		return userservice.getUserByEmailIdandPassword(user).toString();
	}
	
}
