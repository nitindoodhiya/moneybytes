package com.example.demo.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.TickerAPI;
import com.example.demo.entities.Trade;
import com.example.demo.service.APIService;
import com.example.demo.service.TradeService;

@CrossOrigin("*")
@RestController
@RequestMapping("api/trade")
public class TradeController {
	@Autowired
	TradeService tradeservice;
    public static Logger logger = LogManager.getLogger(TradeController.class);

	@Autowired
	APIService apiservice;
	@GetMapping(value = "/working")
	public String testTrade() {
		return "Hello from Trade Department";
	}
	
	@GetMapping(value = "/all")
	public String getAllTrades() {
        logger.info("getting all trades");
		return tradeservice.getAllTrades().toString();
	}
	@GetMapping(value = "/cname/{cname}")
	public String getCompaniesByName(@PathVariable("cname") String cname) {
        logger.info("getting companies smimlar to "+cname);

		return tradeservice.getCompaniesByName(cname).toString();
	}
	
	@GetMapping(value = "/tickerName/{tname}")
	public String getAllByTickerName(@PathVariable("tname") String tickername) {
        logger.info("getting all Traddes done by TickerName "+ tickername);
		return tradeservice.getAllByTickerName(tickername).toString();
	}
	
	@GetMapping(value = "/user/{userid}")
	public String getAllByTradesByUser(@PathVariable("userid") int userid) {
        logger.info("get all Trades done by user "+userid);
		return tradeservice.getAllByTradesByUser(userid).toString();
	}
	
	@PostMapping(value = "/buy")
	public String buyTrade(@RequestBody Trade trade) {
        logger.info("Buying Trade "+ new JSONObject(trade).toString());

		return tradeservice.buyTrade(trade).toString();
	}
	
	@PostMapping(value = "/sell")
	public String sellTrade(@RequestBody Trade trade) {
        logger.info("Selling Trade "+ new JSONObject(trade).toString());

		return tradeservice.sellTrade(trade).toString();
	}
	@PostMapping(value = "/enquiry")
	public String doEnquiry(@RequestBody TickerAPI tickerapi) {
        logger.info("Doing Enquiry using external API By POST Req"+ new JSONObject(tickerapi).toString());
		return apiservice.doAPIRequest(tickerapi);
	}
	@PostMapping(value = "/autobuy")
	public String autobuyTrade(@RequestBody Trade trade) {
        logger.info("Auto Buying Trade "+ new JSONObject(trade).toString());

		return tradeservice.autobuyTrade(trade).toString();
	}
	
	@PostMapping(value = "/autosell")
	public String autosellTrade(@RequestBody Trade trade) {
        logger.info("Auto Selling Trade "+ new JSONObject(trade).toString());

		return tradeservice.autosellTrade(trade).toString();
	}
	@GetMapping(value = "/enquiry")
	public String doEnquiry1(@RequestParam(name = "symbol",required=true) String symbol,@RequestParam(name = "interval",required=false) String interval,@RequestParam(name = "date_from",required=false) String date_from,@RequestParam(name = "date_to",required=false) String date_to,@RequestParam(name = "limit",required=false) Integer limit,@RequestParam(name = "offset",required=false) Integer offset,@RequestParam(name = "sort",required=false) String sort){
		try {
			TickerAPI  tickerapi = new TickerAPI(symbol,interval,date_from,date_to,limit,offset,sort);
	        logger.info("Doing Enquiry using external API By Get Req"+ new JSONObject(tickerapi).toString());
			return apiservice.doAPIRequest(tickerapi); 
		} catch(Exception ex) {
	        logger.error("Doing Enquiry using external API By POST Req "+ new JSONObject(ex).toString());
			return ex.toString();
		}
	}
	@GetMapping(value = "/enquiry/alltickers")
	public String getAllTickers() {
		try {
			logger.info("Doing Enquiry using external API By Get Req all tickers");
			return apiservice.getAllTickers(); 
		} catch(Exception ex) {
	        logger.error("Doing Enquiry using external API By POST Req "+ new JSONObject(ex).toString());
			return ex.toString();
		}
	}
}
